<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/password/reset',['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@index']);
Route::post('/password/reset',['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@reset']);
Route::get('privacy', ['as' => 'privacy', 'uses' => 'PrivacyController@index']);
Route::get('term', ['as' => 'term', 'uses' => 'TermController@index']);

Route::get('/', ['as' => 'homepage', 'uses' => 'MainController@index']);
Route::get('/{country}', ['as' => 'homepage', 'uses' => 'MainController@getCountryIndex']);
