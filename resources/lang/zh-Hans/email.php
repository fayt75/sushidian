<?php

return [
    'email_reset_password_line_1' => '你好，',
    'email_reset_password_line_2' => '我们收到了您更改速识店帐户密码的请求。 请单击下面的链接重置密码。',
    'email_reset_password_line_3' => '如果您未提出此请求，请立即与我们联系，电邮地址是hello@sushidian.co',
    'email_reset_password_line_4' => '',
    'email_reset_password_line_5' => '速识店团队',
];
