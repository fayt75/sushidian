<?php

return [
    'email_reset_password_line_1' => '你好，',
    'email_reset_password_line_2' => '我們收到了您更改速識店帳戶密碼的請求。請單擊下面的鏈接重置密碼。',
    'email_reset_password_line_2' => '重置密碼',
    'email_reset_password_line_3' => '如果您未提出此請求，請立即與我們聯繫，電郵地址是hello@sushidian.co',
    'email_reset_password_line_4' => '',
    'email_reset_password_line_5' => '速識店團隊',
];
