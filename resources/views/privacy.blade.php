@extends('layouts.master')

@section('page_title', trans('privacy.page_title'))
@section('page_description', trans('index.page_description'))
@section('page_keywords', trans('index.page_keywords'))
@section('meta_title', trans('privacy.page_title'))
@section('meta_description', trans('index.page_description'))
@section('meta_image', asset("img/backgrounds/bg_1.jpg"))

@section('section_main')
    <section id="privacy" class="pt100 pb90">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 feature-left">
                    <h3>隐私权保护政策</h3>

                    <p>最后更新: 1月 26号 2019年</p><br /><br />

                    <p>非常欢迎您光临速识店（以下简称速识店），为了让您能够安心使用速识店的各项服务与资讯，特此向您说明速识店的隐私权保护政策，以保障您的权益，请您详阅下列内容：</p>

                    <br /><h4>一、隐私权保护政策的适用范围</h4><br />

                    <p>隐私权保护政策内容，包括速识店如何处理在您使用网站服务时收集到的个人识别资料。隐私权保护政策不适用于速识店以外的相关连结网站，也不适用于非速识店所委托或参与管理的人员。</p>

                    <br /><h4>二、个人资料的蒐集、处理及利用方式</h4><br />

                    <p>当您造访速识店或使用速识店所提供之功能服务时，我们将视该服务功能性质，请您提供必要的个人资料，并在该特定目的范围内处理及利用您的个人资料；非经您书面同意，速识店不会将个人资料用于其他用途。 速识店在您使用服务信箱、问卷调查等互动性功能时，会保留您所提供的姓名、电子邮件地址、联络方式及使用时间等。</p>

                    <p>于一般浏览时，伺服器会自行记录相关行径，包括您使用连线设备的IP位址、使用时间、使用的浏览器、浏览及点选资料记录等，做为我们增进网站服务的参考依据，此记录为内部应用，决不对外公佈。 为提供精确的服务，我们会将收集的问卷调查内容进行统计与分析，分析结果之统计数据或说明文字呈现，除供内部研究外，我们会视需要公布统计数据及说明文字，但不涉及特定个人之资料。</p>

                    <br /><h4>三、资料之保护</h4><br />

                    <p>速识店主机均设有防火墙、防毒系统等相关的各项资讯安全设备及必要的安全防护措施，加以保护网站及您的个人资料採用严格的保护措施，只由经过授权的人员才能接触您的个人资料，相关处理人员皆签有保密合约，如有违反保密义务者，将会受到相关的法律处分。 如因业务需要有必要委託其他单位提供服务时，速识店亦会严格要求其遵守保密义务，并且採取必要检查程序以确定其将确实遵守。</p>

                    <br /><h4>四、网站对外的相关连结</h4><br />

                    <p>速识店的网页提供其他网站的网路连结，您也可经由速识店所提供的连结，点选进入其他网站。但该连结网站不适用速识店的隐私权保护政策，您必须参考该连结网站中的隐私权保护政策。</p>

                    <br /><h4>五、与第三人共用个人资料之政策</h4><br />

                    <p>速识店绝不会提供、交换、出租或出售任何您的个人资料给其他个人、团体、私人企业或公务机关，但有法律依据或合约义务者，不在此限。</p>

                    <p>前项但书之情形包括不限于：</p>

                    <ul class="bullet-list">
                        <li>i. 经由您书面同意。</li>
                        <li>ii. 法律明文规定。</li>
                        <li>iii. 为免除您生命、身体、自由或财产上之危险。</li>
                        <li>iv. 与公务机关或学术研究机构合作，基于公共利益为统计或学术研究而有必要，且资料经过提供者处理或蒐集著依其揭露方式无从识别特定之当事人。 </li>
                        <li>v. 当您在网站的行为，违反服务条款或可能损害或妨碍网站与其他使用者权益或导致任何人遭受损害时，经网站管理单位研析揭露您的个人资料是为了辨识、联络或採取法律行动所必要者。</li>
                        <li>vi. 有利于您的权益。</li>
                        <li>vii. 速识店委託厂商协助蒐集、处理或利用您的个人资料时，将对委外厂商或个人善尽监督管理之责。</li>
                    </ul>

                    <br /><h4>六、隐私权保护政策之修正</h4><br />

                    <p>速识店隐私权保护政策将因应需求随时进行修正，修正后的条款将刊登于网站上。</p>
                </div>
            </div>
        </div>
    </section>
@endsection

