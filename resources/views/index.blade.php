@extends('layouts.master')

@section('page_title', trans('index.page_title'))
@section('page_description', trans('index.page_description'))
@section('page_keywords', trans('index.page_keywords'))
@section('meta_title', trans('index.page_title'))
@section('meta_description', trans('index.page_description'))
@section('meta_image', asset("img/backgrounds/bg-hero.jpg"))

@section('section_slide')
    <!-- Hero -->
    <section id="hero" class="hero-fullwidth parallax" data-overlay-dark="7">
        <div class="background-image">
            <img src="img/backgrounds/bg-1.jpg" alt="#">
        </div>

        <div class="container">
            <div class="row">

                <div class="hero-content-slider mt20" data-autoplay="true" data-speed="4000">
                    <div>
                        <h1>@lang('index.hero_title_1')</h1>
                        <h3>@lang('index.hero_subtitle_1')</h3>
                        <a href="#subscription" class="btn btn-lg btn-primary btn-scroll">@lang('index.hero_description_1')</a>
                    </div>
                    <div>
                        <h1>@lang('index.hero_title_2')</h1>
                        <h3>@lang('index.hero_subtitle_2')</h3>
                        <a href="#subscription" class="btn btn-lg btn-primary btn-scroll">@lang('index.hero_description_2')</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Hero -->
@endsection

@section('section_main')
    <!-- About Us -->
    <section id="about" class="pt100 pb90">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center pb20">
                    <h2>@lang('index.text_path_to_success')<br><strong>@lang('index.text_must_have_tool')</strong></h2>
                    <p class="lead">@lang('index.text_marketing_1')</p>
                </div>

                <div class="col-sm-4 feature-left">
                    <i class="icon-clock size-3x color"></i>
                    <i class="icon-clock back-icon"></i>
                    <div class="feature-left-content">
                        <h4>@lang('index.about_title_1')<br><small>@lang('index.about_description_1')</small></h4>
                        <p class="about-description"></p>
                    </div>
                </div>
                <div class="col-sm-4 feature-left">
                    <i class="icon-genius size-3x color"></i>
                    <i class="icon-genius back-icon"></i>
                    <div class="feature-left-content">
                        <h4>@lang('index.about_title_2')<br><small>@lang('index.about_description_2')</small></h4>
                        <p class="about-description"></p>
                    </div>
                </div>
                <div class="col-sm-4 feature-left">
                    <i class="icon-lightbulb size-3x color"></i>
                    <i class="icon-lightbulb back-icon"></i>
                    <div class="feature-left-content">
                        <h4>@lang('index.about_title_3')<br><small>@lang('index.about_description_3')</small></h4>
                        <p class="about-description"></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Us -->

    <!-- Subscription Forms -->
    {{--<section id="subscription" class="bg-grey" data-overlay-dark="8">--}}
        {{--<div class="container pt110 pb90">--}}
            {{--<div class="row text-center">--}}

                {{--<div class="col-md-12 text-center">--}}
                    {{--<h2 id="subscribe-title"><strong>@lang('index.subscription_title_1')</strong></h2>--}}
                    {{--<p id="subscribe-description" class="lead">@lang('index.subscription_description_1')<span class="color">@lang('index.subscription_description_2')</span>@lang('index.subscription_description_3')<span class="color">@lang('index.subscription_description_4')</span>@lang('index.subscription_description_5')<span class="color">{{ $rate ?? '-' }}</span>@lang('index.subscription_description_6')</p>--}}
                {{--</div>--}}

                {{--<div class="subscription">--}}
                    {{--<br />--}}
                    {{--<div class="input-group">--}}
                        {{--{{ Form::open(['id' => 'subscribe-form', 'onsubmit' => 'return false']) }}--}}
                        {{--{{ csrf_field() }}--}}
                        {{--<div class="form-validation alert"></div>--}}
                        {{--<div class="form-group subscribe-form-input">--}}
                            {{--<span class="input-group-btn subscribe-top">--}}
                                {{--<input id="email" type="email" name="email" id="subscribe-form-email" class="top-subscribe-input" placeholder="输入您的电邮"/>--}}
                            {{--</span>--}}
                            {{--<span class="input-group-btn sign-btn">--}}
                                {{--<button id="sign-up" disabled class="subscribe-form-submit btn btn-primary" data-loading-text="@lang('index.text_signing_up')">@lang('index.text_sign_up')</button>--}}
                            {{--</span>--}}
                        {{--</div>--}}
                        {{--{{ Form::close() }}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    <!-- End Subscription Forms -->
@endsection

@section('after_scripts')
    <script>
        // $('#email').on('input', function() {
        //     var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        //
        //     if (testEmail.test($('#email').val())){
        //         $("#sign-up").prop('disabled', false);
        //     }else{
        //         $("#sign-up").prop('disabled', "disabled");
        //     }
        // });

        // $("#sign-up").click(function(){
        //     var data = $('form#subscribe-form').serialize();
        //
        //     $.ajax({
        //         type: "POST",
        //         url: 'api/subscription/store',
        //         data: data,
        //         dataType: 'json',
        //         success: function(data, textStatus, jqXHR){
        //             if (data.success){
        //                 // disable subcription fields
        //                 $("#email").attr("disabled", "disabled");
        //                 $('#sign-up').attr("disabled", "disabled");
        //                 $('#sign-up').html(data.message_1);
        //                 $('#subscribe-title').html(data.message_1);
        //                 $('#subscribe-description').html(data.message_2);
        //             }
        //         },
        //         error: function(xhr, status, error) {
        //
        //         }
        //     });
        // });
    </script>
@endsection