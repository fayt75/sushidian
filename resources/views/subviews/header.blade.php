<div class="pre-loader"><div></div></div>

<!-- Start Header -->
<nav class="navbar nav-down" data-fullwidth="true" data-menu-style="transparent" data-animation="hiding"><!-- Styles: light, dark, transparent | Animation: hiding, shrink -->
    <div class="container">

        <div class="navbar-header">
            <div class="container">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar top-bar"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
                </button>
                <a class="navbar-brand to-top" href="/"><img src="{{asset("img/assets/logo-light.png")}}" class="logo-light" alt="#"><img src="{{asset("img/assets/logo-dark.png")}}" class="logo-dark" alt="#"></a>
            </div>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
            <div class="container">
                <ul class="nav navbar-nav menu-right">
                    <li class="nav-separator"></li>
                    <li class="nav-icon"><a href="https://www.facebook.com/sushidian" target="_blank"><i class="ion-social-facebook"></i></a></li>
                    <!-- <li class="nav-icon"><a href="mailto:me@shinchong.com"><i class="ion-email"></i></a></li> -->
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- End Header -->
