<link href="{{asset("img/assets/favicon.ico")}}" rel="icon">
<link href="{{asset("css/init.css")}}" rel="stylesheet" type="text/css">
<link href="{{asset("css/ion-icons.min.css")}}" rel="stylesheet" type="text/css">
<link href="{{asset("css/etline-icons.min.css")}}" rel="stylesheet" type="text/css">
<link href="{{asset("css/theme.css")}}" rel="stylesheet" type="text/css">
<link href="{{asset("css/custom.css")}}" rel="stylesheet" type="text/css">
<link href="{{asset("css/colors/green-2.css")}}" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CRaleway:400,100,200,300%7CHind:400,300" rel="stylesheet" type="text/css">