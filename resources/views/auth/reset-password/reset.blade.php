@extends('layouts.master')

@section('page_title', 'confess.today | Reset Password')
@section('page_description', 'confess.today | Reset Password')
@section('page_keywords', '')
@section('meta_title', 'confess.today | Reset Password')
@section('meta_description', 'confess.today | Reset Password')
@section('meta_image', asset("img/backgrounds/img/bg_1.jpg"))

@section('section_main')
    <section id="contact-form" class="pt100 pb90">
        <div class="container text-center">
            <div class="row">
                <div class="col-lg-12 section-title-price wow flipInX">
                    <h2><small>Reset Password</small></h2>
                </div>
                <div class="col-lg-12 text-center wow flipInX" id="contact">
                    @if (\Illuminate\Support\Facades\Session::has('success'))
                        <div id="message">{{ \Illuminate\Support\Facades\Session::get('success') }}</div>
                    @endif

                    @if (\Illuminate\Support\Facades\Session::has('error'))
                        <div id="message">{{ \Illuminate\Support\Facades\Session::get('error') }}</div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input name="password" type="password" id="password" size="30" placeholder="Password" class="form-control" required />

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <br />
                            <input type="submit" class="submit" name="formSubmit" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection



