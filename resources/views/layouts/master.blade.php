<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122648821-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-122648821-1');
        </script>
        
        <!-- Meta Data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content='{{ trans('app.name') }}'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <meta name="description" content="@yield('page_description')"/>
        <meta name="keywords" content="@yield('page_keywords')"/>
        <!-- default facebook app id, not attach to actual app id -->
        <meta property="fb:app_id" content="966242223397117" />
        <meta property="og:locale" content="{{ Config::get('app.locale') }}" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:title" content="@yield('meta_title')" />
        <meta property="og:description" content="@yield('meta_description')" />
        <meta property="og:image" content="@yield('meta_image')" />
        <meta name="token" content="{{ csrf_token() }}" />

        <title>@yield('page_title')</title>

        @include('subviews.link')

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    </head>
<body data-fade-in="true">
@yield('section_slide')

@include('subviews.header')

<div class="site-wrapper">
    @yield('section_main')

    @include('subviews.footer')
</div>

@include('subviews.javascript')


<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
        }
    });
</script>
    
@yield('after_scripts')

</body>
</html>