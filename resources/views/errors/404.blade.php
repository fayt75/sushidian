@extends('layouts.master')

@section('page_title', 'Page Not Found')


@section('section_main')
        <!--Start About -->
<section id="about">
    <div class="container content">
        <div class="col-sm-12 call-action-1 text-center">
            <div class="col-md-12">
                <h3 class="white"><small class="white">Page Not Found</small></h3>
            </div>
            <div class="col-md-12 action-btn">
                <a href="/" class="btn btn-primary btn-lg btn-responsive">GO TO HOME</a>
            </div>
        </div>
    </div>
</section>
<!--End About-->
</section>
<!-- End Team -->
@endsection