(function($) {
	$.fn.collectcoMaps = function(options) {
		var setting = $.extend({
				zoom: 14,
				overviewMapControl: false,
				streetViewControl: false,
				scrollwheel: true,
				mapTypeControl: true,
				radius: 10,
				locationInput: '.locationInput',
			}, setting),
			allMarkers = [],
			collectionCenterData = null;

		return this.each(function() {
			var _this = this,
				inputName = $(this).attr('id') + '-input';
			/**
			 * add autocomplete input element before maps
			 */
			$(_this).before('<input class="collectcoMaps-input" id="' + inputName + '" type="text" placeholder="Search Location">');

			$.getJSON("data/location.json", function(data) {
				collectionCenterData = data;
				/**
				 * create new maps
				 * @type {google}
				 */
				var map = new google.maps.Map(_this, {
					zoom: setting.zoom,
					center: new google.maps.LatLng(collectionCenterData[0], collectionCenterData[1]),
					scrollwheel: setting.scrollwheel,
					streetViewControl: setting.streetViewControl,
					overviewMapControl: setting.overviewMapControl,
					mapTypeControl: setting.m
						/**
						 * define autocomplete input element
						 */
						apTypeControl
				});
				var inputmapElement = $('#' + inputName);
				inputmap = (inputmapElement[0]);
				map.controls[google.maps.ControlPosition.TOP_LEFT].push(inputmap);
				autocompletemap = new google.maps.places.Autocomplete(inputmap);
				autocompletemap.bindTo("bounds", map);

				autocompletemap.addListener("place_changed", function() {
					/**
					 * reset locationInput if map changed
					 */
					$(setting.locationInput).val('');

					/**
					 * Remove all markers on the maps
					 */
					for (i = 0; i < allMarkers.length; i++) {
						allMarkers[i].setMap(null);
					}

					placemap = autocompletemap.getPlace();
					if (!placemap.geometry) {
						window.alert("Autocomplete returned place contains no geometry");
						return;
					}
					map.setCenter(placemap.geometry.location);
					nearbyLocation = getNearbyLocation(placemap.geometry.location.lat(), placemap.geometry.location.lng());
					$.each(nearbyLocation, function(index, data) {
						if (data) {
							markermaps = new google.maps.Marker({
								position: new google.maps.LatLng(data.location.lat, data.location.lng),
								map: map
							});
							allMarkers.push(markermaps);
							infowindow = new google.maps.InfoWindow();
							google.maps.event.addListener(markermaps, 'click', (function(markermaps, index) {
								return function() {
									var infoWindowData = updateMapTooltip(data.location);
									infowindow.setContent(infoWindowData);
									infowindow.open(map, markermaps);
								}
							})(markermaps, index));
						}
					})
				});
			});

			function getNearbyLocation(Latitude, Longtitude) {
				var i = 0,
					locationObject = [];

				$.each(collectionCenterData, function(index, b) {
					distanceValue = getDistance(Latitude, Longtitude, b.lat, b.lng);
					if (setting.radius >= distanceValue) {
						locationObject[index] = {
							location: collectionCenterData[index],
							distance: distanceValue,
						}
					}
				});

				locationObject.sort(function(a, b) {
					// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
					return parseInt(a.distance) - parseInt(b.distance)
				});
				return locationObject;
			}

			function updateMapTooltip(data) {
				html = '<div class="collectco-infowindow"><h2>' + data.name + '</h2>' +
					'<b>address</b> : ' + data.address + '<br/>' +
					'<b>contact</b> : ' + data.contact_number + '<br/><br/>' +
					'<div class="text-center"><button class="collectcoSelectMarker" data-id="' + data.collection_center_id + '">SELECT THIS COLLECTION CENTER</button></div></div><br/>';
				return html;
			}
			$('body').on('click', '.collectcoSelectMarker', function(e) {
				e.preventDefault();
				$(setting.locationInput).val($(this).data('id'));
				$(this).addClass('green').text('SELECTED');
			});

			function getDistance(currentLatitude, currentLongtitude, lat1, long1) {
				earthRadius = 6371;

				currentLatitude = currentLatitude * (Math.PI / 180);
				currentLongtitude = currentLongtitude * (Math.PI / 180);
				lat1 = lat1 * (Math.PI / 180);
				long1 = long1 * (Math.PI / 180);

				x0 = currentLongtitude * earthRadius * Math.cos(currentLatitude);
				y0 = currentLatitude * earthRadius;

				x1 = long1 * earthRadius * Math.cos(lat1);
				y1 = lat1 * earthRadius;

				dx = x0 - x1;
				dy = y0 - y1;

				d = Math.sqrt((dx * dx) + (dy * dy));
				return d;
			};
		});

	};
}(jQuery));