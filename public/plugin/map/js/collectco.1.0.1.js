;
(function($, window, document, undefined) {
	var pluginName = 'collectcoMaps';

	function Plugin(element, options) {
		this.element = element;
		this._name = pluginName;
		this._defaults = $.fn.collectcoMaps.defaults;
		this.options = $.extend({}, this._defaults, options);
		this.init();
	}

	$.extend(Plugin.prototype, {
		init: function() {
			this.buildCache();
			this.prepareTemplate();
			this.bindEvents();
		},

		// Remove plugin instance completely
		destroy: function() {
			this.unbindEvents();
			this.$element.removeData();
		},

		closeInfoWindow: function() {
			/**
			 * to call this function
			 * $(selector).data('collectcoMaps').closeInfoWindow();
			 */
			this.$infowindow.close();
			this.addValueToElement('');
		},

		// Cache DOM nodes for performance
		buildCache: function() {
			this.$element = $(this.element);
			this.$autocomplete = null;
			this.$map;
			this.$placemap;
			this.$autocompleteMapInput;
			this.$wrapper;
			this.$infowindow;
			this.$allMarkers = [];
			this.$collectionCenterData = null;
		},

		bindEvents: function() {
			var self = this;
			var autocompleteTextField = $(self.$element["0"].previousElementSibling.previousElementSibling);
			var googleMapsPreview = $(self.$element["0"].previousElementSibling);
			this.$wrapper = $(self.$element["0"].parentNode);
			var mapOption = {
				zoom: this.options.zoom,
				scrollwheel: this.options.scrollwheel,
				streetViewControl: this.options.streetViewControl,
				overviewMapControl: this.options.overviewMapControl,
				mapTypeControl: this.options.mapTypeControl
			};
			if (self.options.lat !== null && self.options.lng !== null) {
				mapOption = $.extend($mapOption, {
					center: {
						"lat": this.options.lat,
						"lng": this.options.lng
					}
				});
			}
			this.$map = new google.maps.Map(googleMapsPreview[0], mapOption);
			this.$map.controls[google.maps.ControlPosition.TOP_LEFT].push(autocompleteTextField[0]);
			this.$autocompleteMapInput = new google.maps.places.Autocomplete(autocompleteTextField[0]);
			this.$autocompleteMapInput.bindTo("bounds", this.$map);
			this.$autocompleteMapInput.addListener("place_changed", function() {
				self.processOnMapChange();
			});
			this.showAllCollection();

			autocompleteTextField.focusin(function() {
				$(document).keypress(function(e) {
					if (e.keyCode == 13) {
						e.preventDefault();
					}
				});
			});
		},

		prepareTemplate: function() {
			// hide element
			this.$element.hide();
			// add wrapper
			this.$element.wrap(this.addWrapper());
			// add text-field
			this.$element.before(this.addTextfield());
			// add button to coordinate
			this.$element.before(this.addMapsArea());
		},

		// Unbind events that trigger methods
		unbindEvents: function() {
			this.$element.off('.' + this._name);
		},

		addWrapper: function() {
			return $('<div>').addClass('collectctoWrapper');
		},
		addTextfield: function() {
			return $('<input>').attr({
				type: 'text',
				placeholder: 'Search Location',
				id: 'autocompleteTextField'
			}).addClass('pac-input');
		},
		addMapsArea: function() {
			return $('<div>').attr({
				id: 'googleMaps'
			}).addClass('maps-area');
		},
		showAllCollection: function() {
			var self = this;
			$.getJSON(this.options.collectionCenterPath, function(data) {
				var data = data.data;
				if (self.options.lat == null && self.options.lng == null) {
					self.$map.setCenter(new google.maps.LatLng(data[0].lat, data[0].lng));
				}
				self.$infowindow = new google.maps.InfoWindow();
				$.each(data, function(index, d) {
					markerOptions = {
						position: new google.maps.LatLng(d.lat, d.lng),
						map: self.$map,
						icon: 'https://s3-ap-southeast-1.amazonaws.com/collectco-prd/Image/map_marker_1.png'
					};
					markermaps = new google.maps.Marker(markerOptions);
					self.$allMarkers.push(markermaps);

					if (d.collection_center_id == self.options.showDefaultLocationId) {
						var infoWindowData = self.setInfoWindow(d);
						self.$infowindow.setContent(infoWindowData);
						self.$infowindow.open(self.$map, markermaps);
						setTimeout(function() {
							self.setLocation(self.$wrapper.find('.setLocationCenter'));
						}, 100);
					}

					google.maps.event.addListener(markermaps, 'click', (function(markermaps, index) {
						return function() {
							var infoWindowData = self.setInfoWindow(d);
							self.addValueToElement('');
							self.$infowindow.setContent(infoWindowData);
							self.$infowindow.open(self.$map, markermaps);
							self.$wrapper.find('.setLocationCenter').on("click", function(e) {
								e.preventDefault();
								self.setLocation($(this));
							});
						}
					})(markermaps, index));
				});
			});
		},
		setLocation: function(el) {
			this.addValueToElement(el.data('id'));
			el.addClass('green').text('SELECTED');
		},
		addValueToElement: function(value) {
			this.$element.val(value);
		},
		processOnMapChange: function() {

			/**
			 * reset locationInput if map changed
			 */
			this.$element.val('');

			placemap = this.$autocompleteMapInput.getPlace();
			if (!placemap.geometry) {
				window.alert("Autocomplete returned place contains no geometry");
				return;
			}
			this.$map.setCenter(placemap.geometry.location);

		},
		setInfoWindow: function(data) {
			return this.getInfoWindowContent(data);
		},
		getInfoWindowContent: function(data) {
			var table = $('<table>').addClass('table');
			$.each(data.opening_hours, function(index, content) {
				var tr = $('<tr>');
				$.each(content, function(index, c) {
					var td = $('<td>');
					if (index == 'day') {
						td.addClass('dayName');
					}
					td.html((c == null) ? '-' : c);
					tr.append(td);
				});
				table.append(tr);
			});
			string = '<h4>' + data.name + '</h4>' + '<b>Address</b> : ' + data.address + '<br/>' +
				'<br/><br/>' + '<div class="col-md-12 text-center"><button class="setLocationCenter" data-id="' + data.id + '">SELECT THIS COLLECTION CENTER</button></div><br/>' +
				'<br/><b>Opening Hours</b> : <br /><br />' + table.prop('outerHTML');
			return $('<div>').addClass('collectcoInfowindow').html(string).prop('outerHTML');
		}
	});

	$.fn.collectcoMaps = function(options) {
		this.each(function() {
			if (!$.data(this, pluginName)) {
				$.data(this, pluginName, new Plugin(this, options));
			}
		});
		return this;
	};
	$.fn.collectcoMaps.defaults = {
		zoom: 14,
		overviewMapControl: false,
		streetViewControl: false,
		scrollwheel: false,
		mapTypeControl: true,
		lat: null,
		lng: null,
		showDefaultLocationId: null,
		collectionCenterPath: "data/location.json"
	};
})(jQuery, window, document);