# Collectco Map Plugin

## Configure action

To configure action after collection center action, edit section below

```
$(window).on("load", function(){
   $.ready.then(function(){
		$('.locationInput').collectcoMaps();
   });
});	
```

## Configure data source with collectco API
```
$(window).on("load", function(){
   $.ready.then(function(){
		$('.locationInput').collectcoMaps({
			collectionCenterPath: 'your_collectco_collection_center_api_put_it_here'
		});
   });
});		
```

## Configure data source with local file

To update collection center data, export data from cc_m_collection_center, parse it to json format, and save it in 'data/location.json'

## Show default location on the map with infowindow
```
$(window).on("load", function(){
   $.ready.then(function(){
		$('.locationInput').collectcoMaps({
			showDefaultLocationId: 1 // put your collection center id in here
		});
   });
});		
```

## Option Detail

| Option        | Description    |
| ------------- |:-------------:|
| collectionCenterPath      | Use collectco collection center API url | 
| showDefaultLocationId      | Default location infowindow will be show up when map is loaded      | 
| lat | Default Latitude      | 
| lng | Default Longtitude      | 


## Official Contact

Any issue/feedback on this plugin, please contact hello@collectco.asia

## Credits

Credits to @arryalphacode, father of this plugin.