<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Newsletter;

class MainController extends Controller
{
    public function index()
    {
        return $this->getCountryIndex('my');
    }

    public function getCountryIndex(String $country)
    {
        if ($country != 'my' && $country != 'sg' && $country != 'hk' && $country != 'tw' && $country != 'cn'){
            $country = 'my';
        }

        switch ($country) {
            case 'my':
                App::setLocale('zh-Hans');
                $rate = 'RM170';
                break;
            case 'sg':
                App::setLocale('zh-Hans');
                $rate = 'SDG59';
                break;
            case 'hk':
                App::setLocale('zh-Hant');
                $rate = 'HK$320';
                break;
            case 'tw':
                App::setLocale('zh-Hant');
                $rate = 'TWD1200';
                break;
            case 'cn':
                App::setLocale('zh-Hans');
                $rate = '¥260';
                break;
            default:
                App::setLocale('zh-Hans');
                $rate = 'RM170';
                break;
        }

        return view('index')->with('rate', $rate);
    }

    public function subscribe(Request $request)
    {
        $input = $request->all();
        $email = $input['email'] ?? null;

        Newsletter::subscribe($email);

        echo json_encode(
            array(
                'success' => true,
                'message_1' => Lang::get('index.text_sign_up_success'),
                'message_2' => Lang::get('index.text_sign_up_success_message')
            )
        );
    }
}
