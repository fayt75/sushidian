<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;

class PrivacyController extends Controller
{
    public function index()
    {
       App::setLocale('zh-Hans');

       return view('privacy');
    }
}
