<?php
declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\ViewResetPasswordRequest;
use Sushidian\Core\Repositories\PasswordResetRepository;
use Sushidian\Core\Repositories\UserRepository;

/**
 * Class ResetPasswordController
 * @package App\Http\Controllers\Auth
 */
class ResetPasswordController extends Controller
{

    /**
     * @param ViewResetPasswordRequest $request
     * @return \Illuminate\View\View
     */
    public function index(ViewResetPasswordRequest $request)
    {
        return view('auth.reset-password.reset')->with('token', $request['token']);
    }

    /**
     * @param ResetPasswordRequest $request
     * @param PasswordResetRepository $passwordResetRepository
     * @param UserRepository $userRepository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(ResetPasswordRequest $request, PasswordResetRepository $passwordResetRepository, UserRepository $userRepository)
    {
        // Get user email
        $passwordReset = $passwordResetRepository->get($request->only('token'))->first();

        if (!$passwordReset){
            return redirect()->back()->with('error', 'Invalid request.');
        }

        // Ger user
        $user = $userRepository->get(['email' => $passwordReset->email])->first();

        // Update password
        $user->password = bcrypt($request['password']);
        $userRepository->save($user);

        // Delete password reset
        $passwordResetRepository->delete($passwordReset);

        return redirect()->back()->with('success', 'Password reset success!');
    }
}
