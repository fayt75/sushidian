<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;

class TermController extends Controller
{
    public function index()
    {
        App::setLocale('zh-Hans');

        return view('term');
    }
}
